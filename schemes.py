import phonenumbers
from phonenumbers.phonenumberutil import NumberParseException
from pydantic import BaseModel, field_validator


class NewRecord(BaseModel):
    phone: str
    address: str

    @field_validator("phone")
    @classmethod
    def phone_validator(cls, v: str) -> str:
        try:
            z = phonenumbers.parse(v, "RU")
        except NumberParseException:
            raise ValueError("Phone Number Invalid.")
        if phonenumbers.is_valid_number(z) is False:
            raise ValueError("Phone Number Invalid.")
        return v
