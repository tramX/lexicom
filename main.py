from fastapi import FastAPI

from dao import get_data, set_data
from schemes import NewRecord

app = FastAPI()


@app.get("/check_data")
async def check_data(phone: str) -> str:
    result = await get_data(phone)
    return "Not found" if result is None else result


@app.post("/write_data", response_model=NewRecord)
async def create(new_record: NewRecord):
    await set_data(new_record.phone, new_record.address)
    return new_record


@app.put("/write_data", response_model=NewRecord)
async def create(new_record: NewRecord):
    await set_data(new_record.phone, new_record.address)
    return new_record
