import aioredis
import environ

env = environ.Env(redis=(str, "redis://localhost"))
environ.Env.read_env()


redis = aioredis.from_url(f"redis://{env('redis')}")


async def get_data(key):
    value = await redis.get(key)
    return value


async def set_data(key, value):
    await redis.set(key, value)
