import datetime
import string
from random import choice
from typing import List

import psycopg2

file_types: List[str] = ["mp3", "wav", "zip"]

short_name_count = 700000
full_name_count = 500000
full_name_updated_count = 0


def sql_exec(sql: str):
    cursor.execute(sql)


def delete_tables():
    sql_exec("DROP TABLE short_names")
    sql_exec("DROP TABLE full_names")


def create_tables():
    sql_exec(
        """
    CREATE TABLE IF NOT EXISTS short_names (
    name TEXT NOT NULL,
    status INTEGER
    )
    """
    )

    sql_exec(
        """
    CREATE TABLE IF NOT EXISTS full_names (
    name TEXT NOT NULL,
    status INTEGER NULL
    )
    """
    )


def init_data(count_items: int):
    for i in range(0, count_items):
        file_name = "".join(
            [choice(f"{string.ascii_letters}{string.digits}") for _ in range(10)]
        )
        status: int = choice((0, 1))
        file_type: str = choice(file_types)
        sql = (
            f"INSERT INTO short_names (name, status) VALUES ('{file_name}', '{status}')"
        )
        sql_exec(sql)

        if i > full_name_count:
            continue
        sql = f"INSERT INTO full_names (name) VALUES ('{file_name}.{file_type}')"

        sql_exec(sql)


def join(start_position, limit):
    sql = f"SELECT full_names.name, short_names.name, short_names.status FROM short_names JOIN full_names ON full_names.name LIKE ('%' || short_names.name || '%') LIMIT '{limit}' OFFSET '{start_position}'"
    sql_exec(sql)
    values = [(i[2], i[0]) for i in cursor.fetchall()]
    if len(values) > 0:
        sql = """ UPDATE full_names
                    SET status = %s
                    WHERE name = %s"""
        cursor.executemany(sql, values)
    return len(values)


def join2():
    start_position, limit = 0, 50
    while True:
        sql = f"SELECT name FROM full_names LIMIT '{limit}' OFFSET '{start_position}'"
        sql_exec(sql)
        values = [i[0].split(".")[0] for i in cursor.fetchall()]
        sql = "select status, name from short_names where name IN {}".format(
            tuple(values)
        )
        cursor.execute(sql)
        values = [(i[0], f"{i[1]}%") for i in cursor.fetchall()]
        sql = """ UPDATE full_names
                            SET status = %s
                            WHERE name LIKE %s"""
        cursor.executemany(sql, values)

        start_position += limit
        if start_position > 50000:
            break
        if start_position >= full_name_count:
            break


# Устанавливаем соединение с базой данных
connection = psycopg2.connect(
    dbname="lexicom", user="postgres", password="1qazxsw2", host="localhost"
)
cursor = connection.cursor()

# Удаляем старые тавлицы
# delete_tables()

# Создаем таблицы
# create_tables()
dt_start = datetime.datetime.now()

# Инициализация данных
# init_data(short_name_count)

join2()

for i in range(0, int(short_name_count / 1000)):
    count = join(i * 1000, 1000)
    full_name_updated_count += count
    if full_name_updated_count >= full_name_count:
        break


dt_end = datetime.datetime.now()

print(dt_end - dt_start)

# Сохраняем изменения и закрываем соединение
connection.commit()
connection.close()
